// ignore_for_file: unused_import

import 'package:flutter/material.dart';
import 'package:module_3/edit_page.dart';
import 'package:module_3/register_page1.dart';
import 'package:flutter/services.dart';

class ConfrimPage extends StatefulWidget {
  const ConfrimPage({Key? key}) : super(key: key);

  @override
  State<ConfrimPage> createState() => _ConfrimPageState();
}

class _ConfrimPageState extends State<ConfrimPage> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Scaffold(
        backgroundColor: Color.fromARGB(255, 255, 255, 255),
        appBar: AppBar(
          title: const Text('Confirmation page'),
          backgroundColor: Color.fromARGB(255, 52, 66, 60),
        ), //AppBar
        body: const Center(
          child: Text(
            'Thank you for changing your Password!',
            style: TextStyle(fontSize: 24),
          ), //Text
        ), //Center
      ), //Scaffold
      //Removing Debug Banner
    );
  }
}
